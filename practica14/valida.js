function validar()
{   
    //obteniendo el valor que se puso en el campo text del formulario
    var miCampoTexto = document.getElementById("form-name").value;
    var miCampoCorreo = document.getElementById("form-email").value;
    var miCampoTel = document.getElementById("form-tel").value;
    var miCampoStory = document.getElementById("form-story").value;
    //la condición
    if (miCampoTexto.length == 0 || /^\s+$/.test(miCampoTexto)) 
    {
        alert('Tu nombre no puede ir vacio!');
        return false;
    }

    if (miCampoCorreo.length == 0 || /^\s+$/.test(miCampoCorreo)) 
    {
        alert('Tu correo no puede ir vacio!');
        return false;
    }

    if (miCampoTel.length == 0 || /^\s+$/.test(miCampoTel)) 
    {
        alert('Tu telefono no puede ir vacio!');
        return false;
    }
    else
    {
        if (miCampoTel.length < 10 ||miCampoTel.length > 10) 
        {
            alert('Solo 10 digitos');
            return false;
        }
    }
    if (miCampoStory.length > 300) 
    {
        alert('Trata de usar un maximo de 300 caracteres!');
        return false;
    }
    validaDominio(miCampoCorreo);
    contarSeleccionados();  
    alerta();
//return true;
}

function validaDominio(dominioEmail)
{
    var fields= dominioEmail.split('@');
    var subdominio=fields[1].split('.');
    var dominio=subdominio[1];
    if(dominio=="net"||dominio=="com"||dominio=="org"||dominio=="mx")
    {
        alert("dominio correcto");
        return true;
    }
    else
    {
        alert("dominio: "+dominio+" incorrecto");
        return false;
    }
}

function manejador(myRadio)
{
alert('Nuevo valor: ' + myRadio.value);
document.getElementById("miColor").style.background = myRadio.value;
}

function muestraTalla()
{
    if(document.getElementById('tamanoTalla').value==5)
    {
        alert("talla 23");
    }
    if(document.getElementById('tamanoTalla').value==6)
    {
        alert("talla 24");
    }
    if(document.getElementById('tamanoTalla').value==7)
    {
        alert("talla 25");
    }
    if(document.getElementById('tamanoTalla').value==8)
    {
        alert("talla 26");
    }
    if(document.getElementById('tamanoTalla').value==9)
    {
        alert("talla 27");
    }
    if(document.getElementById('tamanoTalla').value==9)
    {
        alert("talla 28");
    }
    if(document.getElementById('tamanoTalla').value==10)
    {
        alert("talla 29");
    }
    if(document.getElementById('tamanoTalla').value==11)
    {
        alert("talla 30");
    }
    if(document.getElementById('tamanoTalla').value==12)
    {
        alert("talla 31");
    }
}

function alerta()
{
    //validar();
    var mensaje;
    var opcion = confirm("Click en Aceptar o Cancelar para enviar tu regalo");
    if (opcion == true) 
    {
       return true;
    } 
    else 
    {
        return false;
    }
    //document.getElementById("ejemplo").innerHTML = mensaje;
}

function contarSeleccionados()
{
    var cant=0;
    if (document.getElementById('laces').checked)
    {
        cant++;
    }
    if (document.getElementById('logo').checked)
    {
        cant++;
    }
    if (document.getElementById('heels').checked)
    {
        cant++;
    }
    if (document.getElementById('mp3').checked)
    {
        cant++;
    }
    if (cant==0) 
    {
        alert("ninguna caracteristica seleccionada");
        return false;
    }
}